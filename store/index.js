    
    export const state=()=>({
        api:{
            key:process.env.API_KEY,
            url:{
                images:'https://image.tmdb.org/t/p/w500/',
                search:`https://api.themoviedb.org/3/search/multi?api_key=${process.env.API_KEY}&language=en-US&query=KEYWORD&page=1&include_adult=false`,
                popular:`https://api.themoviedb.org/3/movie/popular?api_key=${process.env.API_KEY}&language=en-US&page=1`,
            }
        }
    });

    export const mutations={
        Searchmovie(state,data){
            state.content.search=data;
        }
    };

    export const actions={
        async Searchmovie({state,commit},keyword){
            let {data}=await this.$axios({
                method:'get',
                url:state.api.url.search.replace('KEYWORD',keyword),
                responseType:'json'
            });
            return data; 
        },
        async popularmovies({state,commit}){
            let {data}=await this.$axios({
                method:'get',
                url:state.api.url.popular,
                responseType:'json'
            });
            return data;
        }
    };