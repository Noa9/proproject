import axios from '@nuxtjs/axios'

export default function({env,error},inject){
    const tmdb=axios.create({
        baseURL:'https://api.themoviedb.org/3'
    })
    tmdb.interceptors.request.use((config)=>{
        if(!config.params) config.params={}
        config.params.api_key=process.env.api_key
        return config
    })
    inject('api',{tmdb})
}